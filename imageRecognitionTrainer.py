from PIL import Image

# imagePathName = '/Users/betul/Resimler/ucak.jpeg'
# ucakImage = Image.open(imagePathName)
# ucakImage.show()

labels = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']

from keras.datasets import cifar10

(X_train, y_train), (X_test, y_test) = cifar10.load_data()

#index = int(input('Enter an image index: '))
#displayImage = X_train[index]
#displayLabel = y_train[index][0]

#from matplotlib import pyplot as plt

#redImage = Image.fromarray(displayImage)
#red, green, blue = redImage.split()

#plt.imshow(red, cmap="Reds")
#plt.show()



#plt.imshow(displayImage)
#plt.show()
#print(labels[displayLabel])

#finalImage = Image.fromarray(displayImage)
#finalImage.show()
#print(labels[displayLabel])

from keras .utils import np_utils
newXTrain = X_train.astype('float32')
newXTest = X_test.astype('float32')
newXTrain /= 255
newXTest /= 255

newYTrain = np_utils.to_categorical(y_train)
newYTest = np_utils.to_categorical(y_test)


from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers .convolutional import Conv2D, MaxPooling2D
from keras.optimizers import SGD
from keras.constraints import maxnorm

model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=(32, 32, 3), activation='relu', padding='same', kernel_constraint=maxnorm(3)))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dense(512, activation='relu', kernel_constraint=maxnorm(3)))
model.add(Dropout(0.5))
model.add(Dense(10, activation='softmax'))

model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.01), metrics=['accuracy'])
model.fit(newXTrain, newYTrain, epochs=10, batch_size=32)

import h5py
model.save('egitilenModel.h5')



































