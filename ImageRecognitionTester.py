from PIL import Image
import numpy as np
from keras.models import load_model

imagePathName = '~/Desktop/Images/bike1.jpg'
ucakImage = Image.open(imagePathName)
ucakImage.show()

labels = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']
model = load_model('egitilenModel.h5')
dosyaYolu = input('Resmin dosya yolunu giriniz: ')
girdiResim = Image.open(dosyaYolu)
girdiResim = girdiResim.resize((32, 32), resample=Image.LANCZOS)

resimDizisi = np.array(girdiResim)
resimDizisi = resimDizisi.astype('float32')
resimDizisi /= 255.0
resimDizisi = resimDizisi.reshape(1, 32, 32, 3)
sonuc = model.predict(resimDizisi)
girdiResim.show()
print(labels[np.argmax(sonuc)])

